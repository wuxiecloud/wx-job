CREATE TABLE `wx_job_scheduler_register` (
  `sched_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `sched_name` varchar(256) NOT NULL COMMENT '调度器名称',
  `instance_name` varchar(256) NOT NULL COMMENT '实例名称',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `last_check_time` datetime NOT NULL COMMENT '最后检查时间',
  `sched_ip` varchar(128) DEFAULT NULL COMMENT 'IP',
  `sched_port` int(11) DEFAULT NULL COMMENT '端口',
  PRIMARY KEY (`sched_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wx_job` (
  `job_id` char(32) NOT NULL DEFAULT '' COMMENT '任务编号',
  `schedule_name` varchar(256) DEFAULT NULL COMMENT '调度器名称',
  `job_name` varchar(256) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(256) DEFAULT NULL COMMENT '任务分组',
  `description` varchar(2000) DEFAULT NULL COMMENT '任务描述',
  `job_provider_bean_name` varchar(256) DEFAULT NULL COMMENT '任务提供者beanName',
  `cron_expression` varchar(256) DEFAULT NULL COMMENT '执行时间',
  `parameters` text COMMENT '参数',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `job_status` int(11) DEFAULT NULL COMMENT '任务状态',
  `job_type` int(11) DEFAULT NULL COMMENT '任务类型',
  `repeat_count` int(11) DEFAULT NULL COMMENT '重复次',
  `interval_in_seconds` int(11) DEFAULT NULL COMMENT '间隔多少秒执行一次',
  `success_count` int(11) DEFAULT '0' COMMENT '成功次数',
  `fail_count` int(11) DEFAULT '0' COMMENT '失败次数',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `wx_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `job_id` char(32) DEFAULT NULL,
  `schedule_name` varchar(256) DEFAULT NULL,
  `job_name` varchar(256) DEFAULT NULL,
  `job_group` varchar(256) DEFAULT NULL,
  `schedule_instance` varchar(256) DEFAULT NULL,
  `begin_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `job_result` char(1) DEFAULT NULL COMMENT 'y:成功，f：失败',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1647 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `wx_job_lock` (
  `lock_key` varchar(128) NOT NULL DEFAULT '' COMMENT '锁标识',
  `create_time` datetime DEFAULT NULL COMMENT '锁创建时间',
  `lock_status` char(1) DEFAULT NULL COMMENT '锁状态，s:success,f:fail,r:run',
  `lock_hold` varchar(256) DEFAULT NULL COMMENT '锁持有者',
  `lock_expire` datetime DEFAULT NULL COMMENT '锁过期时间',
  PRIMARY KEY (`lock_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;