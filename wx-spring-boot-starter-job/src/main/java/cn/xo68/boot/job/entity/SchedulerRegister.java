package cn.xo68.boot.job.entity;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;




/** 
 * SchedulerRegister 实体
 * @author 你的名字
 * @date 2018年07月04日 14:33:42
 *	
 */
public class SchedulerRegister implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long schedId;
	private String schedName;
	private String instanceName;
	private Date registerTime;
	private Date lastCheckTime;
	private String schedIp;
	private Integer schedPort;
	
	@Override
	public String toString(){
		return "schedId:"+this.schedId+","+"schedName:"+this.schedName+","+"instanceName:"+this.instanceName+","+"registerTime:"+this.registerTime+","+"lastCheckTime:"+this.lastCheckTime+","+"schedIp:"+this.schedIp+","+"schedPort:"+this.schedPort;
	}
	
	public Long getSchedId() {
		return this.schedId;
	}
	public void setSchedId(Long value) {
		this.schedId = value;
	}
	public String getSchedName() {
		return this.schedName;
	}
	public void setSchedName(String value) {
		this.schedName = value;
	}
	public String getInstanceName() {
		return this.instanceName;
	}
	public void setInstanceName(String value) {
		this.instanceName = value;
	}
	public Date getRegisterTime() {
		return this.registerTime;
	}
	public void setRegisterTime(Date value) {
		this.registerTime = value;
	}
	public Date getLastCheckTime() {
		return this.lastCheckTime;
	}
	public void setLastCheckTime(Date value) {
		this.lastCheckTime = value;
	}
	public String getSchedIp() {
		return this.schedIp;
	}
	public void setSchedIp(String value) {
		this.schedIp = value;
	}
	public Integer getSchedPort() {
		return this.schedPort;
	}
	public void setSchedPort(Integer value) {
		this.schedPort = value;
	}
	
}