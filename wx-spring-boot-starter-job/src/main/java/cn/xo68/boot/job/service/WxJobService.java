package cn.xo68.boot.job.service;

import cn.xo68.boot.job.entity.JobStatus;
import cn.xo68.boot.job.entity.WxJob;
import cn.xo68.boot.job.mapper.WxJobMapper;
import cn.xo68.core.security.Md5Utils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;



/** 
 * WxJob Service
 * @author wuyy(16349023@qq.com)
 * @date 2018年12月11日 19:59:48
 *
 */
 @Service
public class WxJobService{

    private static final String idx_Split="$";

    @Autowired
    private WxJobMapper wxJobMapper;

    public WxJob get(String jobId) {
        return  wxJobMapper.get(jobId);
    }

    public WxJob insert(WxJob wxJob) {
        String jobId= Md5Utils.Encode(wxJob.getScheduleName()+idx_Split +wxJob.getJobGroup()+ idx_Split + wxJob.getJobName());

        wxJob.setJobId(jobId);
        wxJob.setJobStatus(JobStatus.WaitRunning.getCode());
        if(wxJob.getRepeatCount() == null ){
            wxJob.setRepeatCount(0);
        }
        if(wxJob.getIntervalInSeconds() == null || wxJob.getIntervalInSeconds() < 1 ){
            wxJob.setIntervalInSeconds(1);
        }
        wxJob.setSuccessCount(0);
        wxJob.setFailCount(0);

        wxJobMapper.insert(wxJob);
        return wxJob;
    }

    public WxJob update(WxJob wxJob) {
        wxJobMapper.update(wxJob);
        return wxJob;
    }
    public void updateStatus(String jobId, Integer jobStatus){
        wxJobMapper.updateStatus(jobId, jobStatus);
    }
    
    public void delete(String jobId) {
        wxJobMapper.delete(jobId);
    }
    
    public List<WxJob> list() {
        return wxJobMapper.list();
    }
	
	public PageInfo<WxJob> list(int pageNumber,int pageSize) {
        PageHelper.startPage(pageNumber, pageSize,true);
        List<WxJob> docs = wxJobMapper.list();
        PageInfo<WxJob> pageInfo = new PageInfo<>(docs);
        return pageInfo;
    }
}