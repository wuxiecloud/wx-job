package cn.xo68.boot.job.controller;

import cn.xo68.boot.job.entity.QuartzJob;
import cn.xo68.boot.job.entity.SchedulerRegister;
import cn.xo68.boot.job.jdbcstore.JobStoreService;
import cn.xo68.boot.job.jdbcstore.SchedulerRegisterService;
import cn.xo68.core.entity.ResultEntity;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequestMapping("/job")
@RestController
public class JobIndexController {

    @Autowired
    private JobStoreService jobStoreService;

    @Autowired
    private SchedulerRegisterService schedulerRegisterService;

    @PostMapping("/")
    public String add(QuartzJob quartzJob){
        jobStoreService.add(quartzJob);
        return ResultEntity.SUCCESS_MSG;
    }

    @GetMapping("/{jobId}")
    public QuartzJob get(@PathVariable("jobId") String jobId){
        return jobStoreService.getById(jobId);
    }

    @PutMapping("/")
    public String update(@RequestParam(name = "jobId") String jobId,
                               @RequestParam(name = "oldStatus")int oldStatus,
                               @RequestParam(name = "newStatus")int newStatus){
        jobStoreService.update(jobId, oldStatus,newStatus );
        return ResultEntity.SUCCESS_MSG;
    }

    @GetMapping("/list")
    public List<QuartzJob> list(@RequestParam(name = "schedulerName")String schedulerName,
                                              @RequestParam(name = "startStatus")int startStatus,
                                              @RequestParam(name = "endStatus")int endStatus){
        return jobStoreService.listBySchedulerName(schedulerName,startStatus,endStatus);
    }

    @GetMapping("/sched")
    public PageInfo<SchedulerRegister> list(@RequestParam(name="pageIndex",defaultValue="1") int pageIndex, @RequestParam(name="pageSize",defaultValue="10") int pageSize){
        return  schedulerRegisterService.list(pageIndex, pageSize);
    }
}
