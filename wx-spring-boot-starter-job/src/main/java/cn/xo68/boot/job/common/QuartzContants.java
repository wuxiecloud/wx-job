package cn.xo68.boot.job.common;

/**
 * Quartz常量
 * @author wuxie
 * @date 2018-6-21
 */
public class QuartzContants {

    /**
     * 任务数据Key
     */
    public static String DATAMAP_JOB_ENTITY ="quartzJob";

    /**
     * 任务编号
     */
    public static String DATAMAP_JOB_ID="jobId";
    public static String DATAMAP_SCHED_NAME="schedName";
    public static String DATAMAP_JOB_NAME="jobName";
    public static String DATAMAP_JOB_GROUP="jobGroup";
}
