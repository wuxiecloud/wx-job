package cn.xo68.boot.job.entity;

/**
 * 分布式锁状态
 * @author wuxie
 * @Date 2018-7-10
 */
public enum DistributedLockStatus {
    /**
     * 运行
     */
    R,
    /**
     * 成功
     */
    S,
    /**
     * 失败
     */
    F;
}
