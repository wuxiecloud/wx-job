package cn.xo68.boot.job.jdbcstore.impl;

import cn.xo68.boot.job.entity.SchedulerRegister;
import cn.xo68.boot.job.jdbcstore.SchedulerRegisterService;
import cn.xo68.boot.job.mapper.SchedulerRegisterMapper;
import cn.xo68.boot.job.properties.JobProperties;
import cn.xo68.core.date.DateTime;
import cn.xo68.core.jdbc.JdbcOperate;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 调度器注册服务实现
 * @author wuxie
 * @date  2018-7-3
 */
@Service
public class SchedulerRegisterServiceImpl implements SchedulerRegisterService {

    private static final Logger logger=LoggerFactory.getLogger(SchedulerRegisterServiceImpl.class);

    private static final String inserat_sql="insert into wx_job_scheduler_register (sched_name , instance_name , register_time , last_check_time , sched_ip , sched_port ) values (? , ? , ? , ?, ? , ? )";
    private static final String get_sql="SELECT t.* FROM wx_job_scheduler_register t where t.sched_name=? and t.instance_name=?";
    private static final String update_sql="update wx_job_scheduler_register set last_check_time=? where sched_id=?";
    private static final String heartBeatReport_sql="update wx_job_scheduler_register set last_check_time=? where sched_id=?";
    private static final String list_sql="SELECT t.* FROM wx_job_scheduler_register t";

    @Autowired
    private JdbcOperate jdbcOperate;
    @Autowired
    private SchedulerRegisterMapper schedulerRegisterMapper;
    @Autowired
    private JobProperties jobProperties;

//    public SchedulerRegisterServiceImpl(JdbcOperate jdbcOperate) {
//        this.jdbcOperate = jdbcOperate;
//    }

    @Override
    public void register(String schedName, String instanceName, String ip, int port) {
        logger.info(">>> 任务调度器注册... {},{},{},{}",schedName,instanceName,ip,port);
        Date current=new Date();
        List<Object> getArgList=new ArrayList<>();
        getArgList.add(schedName);
        getArgList.add(instanceName);
        SchedulerRegister schedulerRegister=jdbcOperate.get(SchedulerRegister.class,get_sql, getArgList.toArray());
        if(schedulerRegister!=null){
            List<Object> updateArgList=new ArrayList<>();
            updateArgList.add(current);
            //updateArgList.add(current);
            updateArgList.add(schedulerRegister.getSchedId());
            jdbcOperate.update(update_sql, updateArgList.toArray());
        }else {
            List<Object> insertArgList=new ArrayList<>();
            insertArgList.add(schedName);
            insertArgList.add(instanceName);

            insertArgList.add(current);
            insertArgList.add(current);
            insertArgList.add(ip);
            insertArgList.add(port);
            jdbcOperate.update(inserat_sql, insertArgList.toArray());
        }
    }

    @Override
    public void heartBeatReport(String schedName, String instanceName){
        logger.debug(">>> 任务调度器心跳上报... {},{}",schedName,instanceName);
        Date current=new Date();
        List<Object> getArgList=new ArrayList<>();
        getArgList.add(schedName);
        getArgList.add(instanceName);
        SchedulerRegister schedulerRegister=jdbcOperate.get(SchedulerRegister.class,get_sql, getArgList.toArray());
        if(schedulerRegister!=null){
            List<Object> updateArgList=new ArrayList<>();
            updateArgList.add(current);
            updateArgList.add(schedulerRegister.getSchedId());
            jdbcOperate.update(heartBeatReport_sql, updateArgList.toArray());
        }else {
            logger.error("调度器实例 {}-{} 不存在", schedName,instanceName);
        }
    }

    @Override
    public PageInfo<SchedulerRegister> list(int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize,true);
        List<SchedulerRegister> list=schedulerRegisterMapper.list(DateTime.Now().addSecond(jobProperties.getHearBeatPeriod()*-1).getDate());
        PageInfo<SchedulerRegister> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }
}
