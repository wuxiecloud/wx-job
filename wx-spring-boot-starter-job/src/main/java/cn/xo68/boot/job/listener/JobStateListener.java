package cn.xo68.boot.job.listener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobStateListener implements JobListener {

    private final static Logger logger = LoggerFactory.getLogger(JobStateListener.class);

    @Override
    public String getName() {
        return "JobStateListener";
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        logger.debug("=== jobToBeExecuted {}", context.getJobDetail().getKey().getName());

    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext context) {
        logger.debug("=== jobExecutionVetoed {}", context.getJobDetail().getKey().getName());
    }

    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        logger.debug("=== jobWasExecuted {}", context.getJobDetail().getKey().getName());
    }
}
