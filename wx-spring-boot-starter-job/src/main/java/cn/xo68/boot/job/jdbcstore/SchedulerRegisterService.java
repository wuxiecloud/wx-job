package cn.xo68.boot.job.jdbcstore;

import cn.xo68.boot.job.entity.SchedulerRegister;
import com.github.pagehelper.PageInfo;

/**
 * 调度器注册服务
 */
public interface SchedulerRegisterService {

    /**
     * 注册调度器
     * @param schedName
     * @param instanceName
     * @param ip
     * @param port
     */
    void register(String schedName, String instanceName, String ip, int port);

    /**
     * 心跳维护
     * @param schedName
     * @param instanceName
     */
    void heartBeatReport(String schedName, String instanceName);

    PageInfo<SchedulerRegister> list(int pageNumber, int pageSize);
}
