package cn.xo68.boot.job;

import cn.xo68.boot.job.entity.QuartzJob;
import org.quartz.JobExecutionContext;

/**
 * 自定义任务提供者
 * @author wuxie
 * @date 2018-6-22
 */
public interface JobProvider {
    void execute(JobExecutionContext context,QuartzJob quartzJob);
}
