package cn.xo68.boot.job.listener;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TriggerStatusListener implements TriggerListener {

    private final static Logger logger = LoggerFactory.getLogger(TriggerStatusListener.class);

    @Override
    public String getName() {
        return "TriggerStatusListener";
    }

    @Override
    public void triggerFired(Trigger trigger, JobExecutionContext context) {
        logger.debug("=== triggerFired {}", context.getJobDetail().getKey().getName());
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
        return false;
    }

    @Override
    public void triggerMisfired(Trigger trigger) {
        logger.debug("=== triggerMisfired {}", trigger.getJobKey().getName());
    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext context, Trigger.CompletedExecutionInstruction triggerInstructionCode) {
        logger.debug("=== triggerComplete {}, {}", context.getJobDetail().getKey().getName(), triggerInstructionCode);
    }
}
