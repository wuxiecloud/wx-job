package cn.xo68.boot.job.entity;

/**
 * 任务状态
 * @author wuxie
 * @date 2018-7-10
 */
public enum JobStatus {

    WaitRunning(1),
    /**
     * 暂停
     */
    WaitPause(2),
    /**
     * 暂停项重新开始
     */
    WaitResume(3),
    /**
     * 移除
     */
    WaitRemove(4),
    WaitTrigger(5),
    Created(100),
    Running(101),
    Pause(102),
    Resume(103),
    Remove(104),
    Trigger(105),
    Finish(200),
    Exception(500),
    UnKnown(501);

    private final int code;

    JobStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static JobStatus parse(int code){
        for (JobStatus jobStatus: JobStatus.values()) {
            if(jobStatus.getCode()==code){
                return  jobStatus;
            }
        }
        return JobStatus.UnKnown;
    }
}
