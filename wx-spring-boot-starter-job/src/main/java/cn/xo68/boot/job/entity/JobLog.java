package cn.xo68.boot.job.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;




/** 
 * JobLog 实体
 * @author 你的名字
 * @date 2018年07月11日 09:14:24
 *	
 */
public class JobLog {
	private static final long serialVersionUID = 1L;
	private Long logId;
	private String jobId;
	private String scheduleName;
	private String jobName;
	private String jobGroup;
	private String scheduleInstance;
	private Date beginTime;
	private Date endTime;
	private String jobResult;
	
	@Override
	public String toString(){
		return "logId:"+this.logId+","+"scheduleName:"+this.scheduleName+","+"jobName:"+this.jobName+","+"jobGroup:"+this.jobGroup+","+"scheduleInstance:"+this.scheduleInstance+","+"beginTime:"+this.beginTime+","+"endTime:"+this.endTime+","+"jobResult:"+this.jobResult;
	}
	
	public Long getLogId() {
		return this.logId;
	}
	public void setLogId(Long value) {
		this.logId = value;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getScheduleName() {
		return this.scheduleName;
	}
	public void setScheduleName(String value) {
		this.scheduleName = value;
	}
	public String getJobName() {
		return this.jobName;
	}
	public void setJobName(String value) {
		this.jobName = value;
	}
	public String getJobGroup() {
		return this.jobGroup;
	}
	public void setJobGroup(String value) {
		this.jobGroup = value;
	}
	public String getScheduleInstance() {
		return this.scheduleInstance;
	}
	public void setScheduleInstance(String value) {
		this.scheduleInstance = value;
	}
	public Date getBeginTime() {
		return this.beginTime;
	}
	public void setBeginTime(Date value) {
		this.beginTime = value;
	}
	public Date getEndTime() {
		return this.endTime;
	}
	public void setEndTime(Date value) {
		this.endTime = value;
	}
	public String getJobResult() {
		return this.jobResult;
	}
	public void setJobResult(String value) {
		this.jobResult = value;
	}
	
}