package cn.xo68.boot.job.entity;

/**
 * 任务类型
 * @author wuxie
 * @date 2018-7-17
 */
public enum JobType {
    /**
     * 表达式计划
     */
    CronSchedule(0),
    /**
     * 简单计划
     */
    SimpleSchedule(1);

    private final int code;

    JobType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static JobType parse(int code){
        for (JobType jobType: JobType.values()) {
            if(jobType.getCode()==code){
                return  jobType;
            }
        }
        return JobType.CronSchedule;
    }
}
