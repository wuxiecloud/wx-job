package cn.xo68.boot.job.jdbcstore;

import cn.xo68.boot.job.entity.JobLog;

/**
 * 日志存储服务
 */
public interface LogStoreService {
    /**
     * 记录日志
     * @param jobLog
     */
    void add(JobLog jobLog);
}
