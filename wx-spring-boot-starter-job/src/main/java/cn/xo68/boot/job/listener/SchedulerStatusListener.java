package cn.xo68.boot.job.listener;

import cn.xo68.boot.job.common.QuartzContants;
import cn.xo68.boot.job.entity.JobStatus;
import cn.xo68.boot.job.jdbcstore.JobStoreService;
import cn.xo68.boot.job.properties.JobProperties;
import cn.xo68.boot.job.util.JobUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;

/**
 * 调度器监听器
 * @author wuxie
 * @date 2018-7-17
 */
public class SchedulerStatusListener implements SchedulerListener {

    private final static Logger logger = LoggerFactory.getLogger(SchedulerStatusListener.class);

    private final JobStoreService jobStoreService;
    private final JobProperties jobProperties;
    private final ServerProperties serverProperties;
    private final String schedulerInstance;

    public SchedulerStatusListener(JobStoreService jobStoreService, JobProperties jobProperties, ServerProperties serverProperties) {
        this.jobStoreService = jobStoreService;
        this.jobProperties = jobProperties;
        this.serverProperties = serverProperties;
        schedulerInstance= JobUtils.generateSchedulerInstanceName(jobProperties.getIpAddress(),serverProperties.getPort());
    }

    @Override
    public void jobScheduled(Trigger trigger) {
        logger.debug(">>> 任务： {}${}${} 部署调度成功", jobProperties.getSchedulerName(), trigger.getJobKey().getName(), trigger.getJobKey().getGroup());
    }

    @Override
    public void jobUnscheduled(TriggerKey triggerKey) {
        logger.debug(">>> 任务： {}${}${} 卸载调度成功", jobProperties.getSchedulerName(), triggerKey.getName(), triggerKey.getGroup());
    }

    @Override
    public void triggerFinalized(Trigger trigger) {
        logger.debug(">>> 任务： {}${}${} 执行结束", jobProperties.getSchedulerName(), trigger.getJobKey().getName(), trigger.getJobKey().getGroup());
        JobDataMap dataMap = trigger.getJobDataMap();
        try{
            jobStoreService.update(dataMap.getString(QuartzContants.DATAMAP_JOB_ID), JobStatus.Running.getCode(), JobStatus.Finish.getCode());
        }catch (Exception e){
            logger.error("任务执行结束，更新任务状态异常", e);
        }
    }

    @Override
    public void triggerPaused(TriggerKey triggerKey) {
        logger.debug("=== triggerPaused.TriggerKey {}", triggerKey.getName());
    }

    @Override
    public void triggersPaused(String triggerGroup) {
        logger.debug("=== triggersPaused.triggerGroup {}", triggerGroup);
    }

    @Override
    public void triggerResumed(TriggerKey triggerKey) {
        logger.debug("=== triggerResumed.TriggerKey {}", triggerKey.getName());
    }

    @Override
    public void triggersResumed(String triggerGroup) {
        logger.debug("=== triggersResumed.triggerGroup {}", triggerGroup);
    }

    @Override
    public void jobAdded(JobDetail jobDetail) {
        logger.debug("=== jobAdded: {}", jobDetail.getKey().getName());
    }

    @Override
    public void jobDeleted(JobKey jobKey) {
        logger.debug("=== jobDeleted.JobKey: {}", jobKey.getName());
    }

    @Override
    public void jobPaused(JobKey jobKey) {
        logger.debug("=== jobPaused.jobKey: {}", jobKey.getName());
    }

    @Override
    public void jobsPaused(String jobGroup) {
        logger.debug("=== jobsPaused.jobGroup: {}", jobGroup);
    }

    @Override
    public void jobResumed(JobKey jobKey) {
        logger.debug("=== jobResumed.JobKey: {}", jobKey.getName());
    }

    @Override
    public void jobsResumed(String jobGroup) {
        logger.debug("=== jobsResumed.jobGroup: {}", jobGroup);
    }

    @Override
    public void schedulerError(String msg, SchedulerException cause) {

    }

    @Override
    public void schedulerInStandbyMode() {

    }

    @Override
    public void schedulerStarted() {
        logger.info(">>> 调度器：{}${} 启动成功", jobProperties.getSchedulerName(), schedulerInstance);
    }

    @Override
    public void schedulerStarting() {

    }

    @Override
    public void schedulerShutdown() {
        logger.info(">>> 调度器：{}${} 已经关闭", jobProperties.getSchedulerName(), schedulerInstance);
    }

    @Override
    public void schedulerShuttingdown() {

    }

    @Override
    public void schedulingDataCleared() {

    }
}
