package cn.xo68.boot.job.mapper;

import cn.xo68.boot.job.entity.SchedulerRegister;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

public interface SchedulerRegisterMapper {

    @Select("SELECT * FROM wx_job_scheduler_register where last_check_time >= #{lastCheckTime}")
    @Results(value = {
            @Result(id = true, column = "sched_id", property = "schedId"),
            @Result(column = "sched_name", property = "schedName"),
            @Result(column = "instance_name", property = "instanceName"),
            @Result(column = "register_time", property = "registerTime"),
            @Result(column = "last_check_time", property = "lastCheckTime"),
            @Result(column = "sched_ip", property = "schedIp"),
            @Result(column = "sched_port", property = "schedPort")
    })
    List<SchedulerRegister> list(@Param("lastCheckTime") Date lastCheckTime);
}
