package cn.xo68.boot.job.jdbcstore.lock;

import cn.xo68.boot.job.entity.DistributedLock;

import java.util.concurrent.TimeUnit;

/**
 * 分布时锁管理
 * @author wuxie
 * @date 2018-7-9
 */
public interface DistributedLockStore {

    /**
     * 尝试加锁，等待时间内无法取到锁，刚放弃
     * @param lockKey
     * @param lockHold
     * @param timeout
     * @return
     */
    DistributedLock tryLock(String lockKey, String lockHold, long timeout);

    /**
     * 释放锁
     * @param lockKey
     */
    void unLock(String lockKey);
}
