package cn.xo68.boot.job;

import cn.xo68.boot.job.jdbcstore.SchedulerRegisterService;
import cn.xo68.boot.job.jdbcstore.lock.DistributedLockManager;
import cn.xo68.boot.job.properties.JobProperties;
import cn.xo68.core.util.IpUtils;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.core.Ordered;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 任务调度器注册
 */
@Component
public class JobSchedulerRegisterAndOperate implements ApplicationRunner,Ordered {

    private static final Logger logger=LoggerFactory.getLogger(JobSchedulerRegisterAndOperate.class);

    private ScheduledExecutorService scheduledExecutorService=null;

    @Autowired
    private ServerProperties serverProperties;
    @Autowired
    private JobProperties jobProperties;
    @Autowired
    private SchedulerRegisterService schedulerRegisterService;
    @Autowired
    private Scheduler scheduler;
    @Autowired
    private JobOperateService jobOperateService;

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    @Autowired
    private JobSchedulerHeartBeat jobSchedulerHeartBeat;
    @Autowired
    private JobMonitor jobMonitor;

    @Autowired
    private DistributedLockManager distributedLockManager;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        String ip=jobProperties.getIpAddress();
        if(StringUtils.isEmpty(ip)){
            ip=IpUtils.getIp();
        }
        try{
            schedulerRegisterService.register(scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId(), ip, serverProperties.getPort());



            logger.info(">>> 启动调度器，调度器名称：{},调度器实例： {}",scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId());
            schedulerFactoryBean.start();

            scheduledExecutorService=Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.scheduleAtFixedRate(jobSchedulerHeartBeat,30, jobProperties.getHearBeatPeriod(), TimeUnit.SECONDS);
            logger.info(">>> 启动调度器心跳处理：{}，{}",scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId());

            scheduledExecutorService.scheduleAtFixedRate(jobMonitor,2, jobProperties.getJobMonitorPeriod(), TimeUnit.SECONDS);
            logger.info(">>> 启动调度器任务监听：{}，{}",scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId());



        }catch (Exception e){
            logger.error("注册调度器异常", e);
        }

    }

    @Override
    public int getOrder() {
        return 1;
    }
}
