package cn.xo68.boot.job.util;

import cn.xo68.core.util.IpUtils;
import org.springframework.util.StringUtils;

public class JobUtils {

    public static final String INSTANCE_NAME_SPLIT=":";


    public static  String generateSchedulerInstanceName(String ip, int port){
        if(StringUtils.isEmpty(ip)){
            ip=IpUtils.getIp();
        }
        return ip+ INSTANCE_NAME_SPLIT +String.valueOf(port);
    }
}
