package cn.xo68.boot.job.entity;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;




/** 
 * DistributedLock 实体
 * @author wuxie
 * @date 2018年07月09日 17:00:39
 *	
 */
public class DistributedLock implements Serializable {

	private static final long serialVersionUID = 1L;
	private String lockKey;
	private Date createTime;
	private String lockStatus;
	private String lockHold;
	private Date lockExpire;
	
	@Override
	public String toString(){
		return "lockId:"+this.lockKey+","+"createTime:"+this.createTime+","+"lockStatus:"+this.lockStatus+","+"lockHold:"+this.lockHold+","+"lockExpire:"+this.lockExpire;
	}

	public String getLockKey() {
		return lockKey;
	}

	public void setLockKey(String lockKey) {
		this.lockKey = lockKey;
	}

	public Date getCreateTime() {
		return this.createTime;
	}
	public void setCreateTime(Date value) {
		this.createTime = value;
	}
	public String getLockStatus() {
		return this.lockStatus;
	}
	public void setLockStatus(String value) {
		this.lockStatus = value;
	}
	public String getLockHold() {
		return this.lockHold;
	}
	public void setLockHold(String value) {
		this.lockHold = value;
	}
	public Date getLockExpire() {
		return this.lockExpire;
	}
	public void setLockExpire(Date value) {
		this.lockExpire = value;
	}
	
}