package cn.xo68.boot.job.jdbcstore.impl;

import cn.xo68.boot.job.entity.QuartzJob;
import cn.xo68.boot.job.jdbcstore.JobStoreService;
import cn.xo68.core.jdbc.JdbcOperate;
import cn.xo68.core.security.Md5Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务存储服务实现
 */
public class JobStoreServiceImpl implements JobStoreService {

    private static final Logger logger=LoggerFactory.getLogger(JobStoreServiceImpl.class);

    private static final String idx_Split="$";
    private static final String inserat_sql="insert into wx_job ( job_id , schedule_name , job_name , job_group , description , job_provider_bean_name , cron_expression , parameters , start_time , end_time , job_status, job_type,repeat_count,interval_in_seconds ) values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?, ? , ? , ? )";
    private static final String get_sql="SELECT t.* FROM wx_job t where t.job_id=?";
    private static final String update_sql="update wx_job set job_status=?  where job_id=? and job_status=?";
    private static final String list_sql="SELECT t.* FROM wx_job t where t.schedule_name=? and (job_status between ? and ?) limit 10";
    private final JdbcOperate jdbcOperate;

    public JobStoreServiceImpl(JdbcOperate jdbcOperate) {
        this.jdbcOperate = jdbcOperate;
    }

    @Override
    public void add(QuartzJob quartzJob) {

        String jobId=Md5Utils.Encode(quartzJob.getScheduleName()+idx_Split +quartzJob.getJobGroup()+ idx_Split + quartzJob.getJobName());

        quartzJob.setJobId(jobId);

        List<Object> insertArgList=new ArrayList<>();
        insertArgList.add(quartzJob.getJobId());
        insertArgList.add(quartzJob.getScheduleName());

        insertArgList.add(quartzJob.getJobName());
        insertArgList.add(quartzJob.getJobGroup());
        insertArgList.add(quartzJob.getDescription());
        insertArgList.add(quartzJob.getJobProviderBeanName());

        insertArgList.add(quartzJob.getCronExpression());
        insertArgList.add(quartzJob.getParameters());
        insertArgList.add(quartzJob.getStartTime());
        insertArgList.add(quartzJob.getEndTime());
        insertArgList.add(quartzJob.getJobStatus());

        insertArgList.add(quartzJob.getJobType());
        insertArgList.add(quartzJob.getRepeatCount());
        insertArgList.add(quartzJob.getIntervalInSeconds());
        jdbcOperate.update(inserat_sql, insertArgList.toArray());
    }

    @Override
    public QuartzJob getById(String jobId) {
        List<Object> getArgList=new ArrayList<>();
        getArgList.add(jobId);
        QuartzJob quartzJob=jdbcOperate.get(QuartzJob.class,get_sql, getArgList.toArray());
        return quartzJob;
    }

    @Override
    public void update(String jobId, int oldStatus, int newStatus) {
        List<Object> insertArgList=new ArrayList<>();
        insertArgList.add(newStatus);
        insertArgList.add(jobId);
        insertArgList.add( oldStatus);
        jdbcOperate.update(update_sql, insertArgList.toArray());
    }

    @Override
    public List<QuartzJob> listBySchedulerName(String schedulerName, int startStatus, int endStatus) {
        List<Object> getArgList=new ArrayList<>();
        getArgList.add(schedulerName);
        getArgList.add(startStatus);
        getArgList.add(endStatus);
        List<QuartzJob> list=jdbcOperate.queryList(QuartzJob.class,list_sql, getArgList.toArray());
        return list;
    }
}
