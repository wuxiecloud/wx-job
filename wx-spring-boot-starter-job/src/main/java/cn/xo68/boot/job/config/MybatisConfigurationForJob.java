package cn.xo68.boot.job.config;

import cn.xo68.core.util.StringTools;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 *  mybatis 配置类
 * @author wuxie
 * @date 2018/8/4 10:17
 *
 */
@Configuration
@EnableConfigurationProperties({MybatisProperties.class})
@MapperScan(basePackages = "cn.xo68.boot.job.mapper", sqlSessionTemplateRef  = "jobSqlSessionTemplate")
public class MybatisConfigurationForJob {

    @Autowired
    private Environment env;

    @Autowired
    private MybatisProperties mybatisProperties;

//    @ConditionalOnMissingBean(name={"authDataSource"})
//    @Bean(name = "authDataSource")
//    @ConfigurationProperties(prefix = "spring.datasource.hikari")
//    public DataSource authDataSource() {
//        return DataSourceBuilder.create().type(HikariDataSource.class).build();
//    }

    /**
     * 根据数据源创建SqlSessionFactory
     */
    @Bean("jobSqlSessionFactory")
    public SqlSessionFactory jobSqlSessionFactory(DataSource dataSource) throws Exception {
        PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver = new PathMatchingResourcePatternResolver();
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        // 指定数据源(这个必须有，否则报错)
        // 下边两句仅仅用于*.xml文件，如果整个持久层操作不需要使用到xml文件的话（只用注解就可以搞定），则不加
        // 指定基包
        sqlSessionFactoryBean.setTypeAliasesPackage("cn.xo68.boot.job.entity");

        if(StringTools.isNotEmpty(env.getProperty("mybatis.mapperLocations"))){
            sqlSessionFactoryBean.setMapperLocations(pathMatchingResourcePatternResolver.getResources(env.getProperty("mybatis.mapperLocations")));
        }
        if(StringTools.isNotEmpty(mybatisProperties.getConfigLocation())){
            sqlSessionFactoryBean.setConfigLocation(pathMatchingResourcePatternResolver.getResource(mybatisProperties.getConfigLocation()));
        }


        return sqlSessionFactoryBean.getObject();
    }
    @Bean("jobSqlSessionTemplate")
    public SqlSessionTemplate jobSqlSessionTemplate(@Qualifier("jobSqlSessionFactory") SqlSessionFactory jobSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(jobSqlSessionFactory);
    }

    /**
     * 配置事务管理器
     */
    @Bean
    public DataSourceTransactionManager jobTransactionManager(DataSource dataSource) throws Exception {
        return new DataSourceTransactionManager(dataSource);
    }
}
