package cn.xo68.boot.job.mapper;


import cn.xo68.boot.job.entity.WxJob;
import org.apache.ibatis.annotations.*;
import java.util.List;

/** 
 * WxJob mybatis mapper
 * @author wuyy(16349023@qq.com)
 * @date 2018年12月11日 19:59:48
 *
 */
public interface WxJobMapper {


	@Select("SELECT * FROM wx_job WHERE job_id = #{jobId}")
    @Results(value = { 
    @Result(id = true, column = "job_id", property = "jobId"),
        @Result(column = "schedule_name", property = "scheduleName"),
        @Result(column = "job_name", property = "jobName"),
        @Result(column = "job_group", property = "jobGroup"),
        @Result(column = "description", property = "description"),
        @Result(column = "job_provider_bean_name", property = "jobProviderBeanName"),
        @Result(column = "cron_expression", property = "cronExpression"),
        @Result(column = "parameters", property = "parameters"),
        @Result(column = "start_time", property = "startTime"),
        @Result(column = "end_time", property = "endTime"),
        @Result(column = "job_status", property = "jobStatus"),
        @Result(column = "job_type", property = "jobType"),
        @Result(column = "repeat_count", property = "repeatCount"),
        @Result(column = "interval_in_seconds", property = "intervalInSeconds"),
        @Result(column = "success_count", property = "successCount"),
        @Result(column = "fail_count", property = "failCount")
    })
    WxJob get(@Param("jobId") String jobId);
	
	@Select("SELECT * FROM wx_job WHERE job_id = #{jobId}")
    @Results(value = { 
    @Result(id = true, column = "job_id", property = "jobId"),
        @Result(column = "schedule_name", property = "scheduleName"),
        @Result(column = "job_name", property = "jobName"),
        @Result(column = "job_group", property = "jobGroup"),
        @Result(column = "description", property = "description"),
        @Result(column = "job_provider_bean_name", property = "jobProviderBeanName"),
        @Result(column = "cron_expression", property = "cronExpression"),
        @Result(column = "parameters", property = "parameters"),
        @Result(column = "start_time", property = "startTime"),
        @Result(column = "end_time", property = "endTime"),
        @Result(column = "job_status", property = "jobStatus"),
        @Result(column = "job_type", property = "jobType"),
        @Result(column = "repeat_count", property = "repeatCount"),
        @Result(column = "interval_in_seconds", property = "intervalInSeconds"),
        @Result(column = "success_count", property = "successCount"),
        @Result(column = "fail_count", property = "failCount")
    })
    WxJob find(@Param("jobId") String jobId);
	
	@Select("SELECT * FROM wx_job")
    @Results(value = { 
    @Result(id = true, column = "job_id", property = "jobId"),
        @Result(column = "schedule_name", property = "scheduleName"),
        @Result(column = "job_name", property = "jobName"),
        @Result(column = "job_group", property = "jobGroup"),
        @Result(column = "description", property = "description"),
        @Result(column = "job_provider_bean_name", property = "jobProviderBeanName"),
        @Result(column = "cron_expression", property = "cronExpression"),
        @Result(column = "parameters", property = "parameters"),
        @Result(column = "start_time", property = "startTime"),
        @Result(column = "end_time", property = "endTime"),
        @Result(column = "job_status", property = "jobStatus"),
        @Result(column = "job_type", property = "jobType"),
        @Result(column = "repeat_count", property = "repeatCount"),
        @Result(column = "interval_in_seconds", property = "intervalInSeconds"),
        @Result(column = "success_count", property = "successCount"),
        @Result(column = "fail_count", property = "failCount")
    })
    List<WxJob> list();
	
	@Insert("insert into wx_job (job_id ,schedule_name ,job_name ,job_group ,description ,job_provider_bean_name ,cron_expression ,parameters ,start_time ,end_time ,job_status ,job_type ,repeat_count ,interval_in_seconds ,success_count ,fail_count ) values (#{jobId} , #{scheduleName} , #{jobName} , #{jobGroup} , #{description} , #{jobProviderBeanName} , #{cronExpression} , #{parameters} , #{startTime} , #{endTime} , #{jobStatus} , #{jobType} , #{repeatCount} , #{intervalInSeconds} , #{successCount} , #{failCount}  )")
    @Options(useGeneratedKeys = true, keyProperty = "jobId")
    void insert(WxJob wxJob);
	
	@Update("update wx_job set  schedule_name=#{scheduleName}, job_name=#{jobName}, job_group=#{jobGroup}, description=#{description}, job_provider_bean_name=#{jobProviderBeanName}, cron_expression=#{cronExpression}, parameters=#{parameters}, start_time=#{startTime}, end_time=#{endTime}, job_type=#{jobType}, repeat_count=#{repeatCount}, interval_in_seconds=#{intervalInSeconds} where job_id = #{jobId}")
    void update(WxJob wxJob);

    @Update("update wx_job set job_status=#{jobStatus} where job_id = #{jobId}")
	void updateStatus(@Param("jobId") String jobId, @Param("jobStatus") Integer jobStatus);

	@Delete("delete FROM wx_job WHERE job_id = #{jobId}")
	void delete(@Param("jobId") String jobId);
}