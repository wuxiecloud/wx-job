package cn.xo68.boot.job.jdbcstore;

import cn.xo68.boot.job.entity.QuartzJob;

import java.util.List;

/**
 * Job存储服务
 */
public interface JobStoreService {

    void add(QuartzJob quartzJob);

    QuartzJob getById(String jobId);

    void update(String jobId,int oldStatus,int newStatus);

    List<QuartzJob> listBySchedulerName(String schedulerName, int startStatus, int endStatus);

}
