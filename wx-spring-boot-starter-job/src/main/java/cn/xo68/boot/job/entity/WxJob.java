package cn.xo68.boot.job.entity;


import java.util.Date;



/** 
 * WxJob 实体
 * @author wuyy(16349023@qq.com)
 * @date 2018年12月11日 19:59:48
 *
 */
public class WxJob implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 任务编号
	 */
	private String jobId;
	/**
	 * 调度器名称
	 */
	private String scheduleName;
	/**
	 * 任务名称
	 */
	private String jobName;
	/**
	 * 任务分组
	 */
	private String jobGroup;
	/**
	 * 任务描述
	 */
	private String description;
	/**
	 * 任务提供者beanName
	 */
	private String jobProviderBeanName;
	/**
	 * 执行时间
	 */
	private String cronExpression;
	/**
	 * 参数
	 */
	private String parameters;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 任务状态
	 */
	private Integer jobStatus;
	/**
	 * 任务类型
	 */
	private Integer jobType;
	/**
	 * 重复次
	 */
	private Integer repeatCount;
	/**
	 * 间隔多少秒执行一次
	 */
	private Integer intervalInSeconds;
	/**
	 * 成功次数
	 */
	private Integer successCount;
	/**
	 * 失败次数
	 */
	private Integer failCount;
	
	@Override
	public String toString(){
		return "jobId:"+this.jobId+","+"scheduleName:"+this.scheduleName+","+"jobName:"+this.jobName+","+"jobGroup:"+this.jobGroup+","+"description:"+this.description+","+"jobProviderBeanName:"+this.jobProviderBeanName+","+"cronExpression:"+this.cronExpression+","+"parameters:"+this.parameters+","+"startTime:"+this.startTime+","+"endTime:"+this.endTime+","+"jobStatus:"+this.jobStatus+","+"jobType:"+this.jobType+","+"repeatCount:"+this.repeatCount+","+"intervalInSeconds:"+this.intervalInSeconds+","+"successCount:"+this.successCount+","+"failCount:"+this.failCount;
	}
	

	public String getJobId() {
		return this.jobId;
	}
	public void setJobId(String value) {
		this.jobId = value;
	}	

	public String getScheduleName() {
		return this.scheduleName;
	}
	public void setScheduleName(String value) {
		this.scheduleName = value;
	}	

	public String getJobName() {
		return this.jobName;
	}
	public void setJobName(String value) {
		this.jobName = value;
	}	

	public String getJobGroup() {
		return this.jobGroup;
	}
	public void setJobGroup(String value) {
		this.jobGroup = value;
	}	

	public String getDescription() {
		return this.description;
	}
	public void setDescription(String value) {
		this.description = value;
	}	

	public String getJobProviderBeanName() {
		return this.jobProviderBeanName;
	}
	public void setJobProviderBeanName(String value) {
		this.jobProviderBeanName = value;
	}	

	public String getCronExpression() {
		return this.cronExpression;
	}
	public void setCronExpression(String value) {
		this.cronExpression = value;
	}	

	public String getParameters() {
		return this.parameters;
	}
	public void setParameters(String value) {
		this.parameters = value;
	}	

	public Date getStartTime() {
		return this.startTime;
	}
	public void setStartTime(Date value) {
		this.startTime = value;
	}	

	public Date getEndTime() {
		return this.endTime;
	}
	public void setEndTime(Date value) {
		this.endTime = value;
	}	

	public Integer getJobStatus() {
		return this.jobStatus;
	}
	public void setJobStatus(Integer value) {
		this.jobStatus = value;
	}	

	public Integer getJobType() {
		return this.jobType;
	}
	public void setJobType(Integer value) {
		this.jobType = value;
	}	

	public Integer getRepeatCount() {
		return this.repeatCount;
	}
	public void setRepeatCount(Integer value) {
		this.repeatCount = value;
	}	

	public Integer getIntervalInSeconds() {
		return this.intervalInSeconds;
	}
	public void setIntervalInSeconds(Integer value) {
		this.intervalInSeconds = value;
	}	

	public Integer getSuccessCount() {
		return this.successCount;
	}
	public void setSuccessCount(Integer value) {
		this.successCount = value;
	}	

	public Integer getFailCount() {
		return this.failCount;
	}
	public void setFailCount(Integer value) {
		this.failCount = value;
	}	
}