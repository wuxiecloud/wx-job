package cn.xo68.boot.job.controller;

import cn.xo68.boot.job.entity.JobStatus;
import cn.xo68.boot.job.entity.WxJob;
import cn.xo68.boot.job.service.WxJobService;
import cn.xo68.boot.webcore.common.ResponseResultConstants;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/** 
 * WxJob Service
 * @author wuyy(16349023@qq.com)
 * @date 2018年12月11日 19:59:48
 *
 */
@RequestMapping("/WxJob")
@RestController
public class WxJobController{
    @Autowired
    private WxJobService wxJobService;

	@GetMapping("/find")
    public WxJob find(@RequestParam(name="id") String id) {
        return wxJobService.get(id);
    }
    
    @GetMapping("/list")
    public PageInfo<WxJob> list(@RequestParam(name="pageIndex",defaultValue="1") int pageIndex,@RequestParam(name="pageSize",defaultValue="10") int pageSize){
        return wxJobService.list(pageIndex, pageSize);
    }
	
	@PostMapping("/add")
    public String add(@RequestBody WxJob entity ) {
        wxJobService.insert(entity);
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
    }
    @PostMapping("/delete")
    public String delete(@RequestParam(name="id") String id) {
        wxJobService.delete(id);
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
    }
	@PostMapping("/stop")
	public String stop(@RequestParam(name="id") String id) {
	    wxJobService.updateStatus(id, JobStatus.WaitRemove.getCode());
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
	}
    @PostMapping("/restart")
    public String restart(@RequestParam(name="id") String id) {
        wxJobService.updateStatus(id, JobStatus.WaitRunning.getCode());
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
    }
    @PostMapping("/pause")
    public String pause(@RequestParam(name="id") String id) {
        wxJobService.updateStatus(id, JobStatus.WaitPause.getCode());
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
    }
    @PostMapping("/resume")
    public String resume(@RequestParam(name="id") String id) {
        wxJobService.updateStatus(id, JobStatus.WaitResume.getCode());
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
    }
	
	@PostMapping("/update")
	public String update(@RequestBody WxJob entity) {
	    wxJobService.update(entity);
        return ResponseResultConstants.RESPONSE_DATA_SUCCESS;
	}
}