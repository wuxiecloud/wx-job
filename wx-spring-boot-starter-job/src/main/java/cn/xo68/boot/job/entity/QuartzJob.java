package cn.xo68.boot.job.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 任务实体
 * @author wx
 * @date 2018-6-11
 */
public class QuartzJob implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 任务编号
     */
	private String jobId;

	/**
	 * 调度器名称
	 */
	private String scheduleName;
	/**
	 * 任务名称
	 */
	private String jobName;
	/**
	 * 任务分组
	 */
	private String jobGroup;
	/**
	 * 任务描述
	 */
	private String description;
	/**
	 * 任务提供者beanName
	 */
	private String jobProviderBeanName;
	/**
	 * 执行时间
	 */
	private String cronExpression;

	/**
	 * 参数
	 */
	private String parameters;

	private Date startTime;
	private Date endTime;

	private int jobStatus;

    /**
     * 任务类型
     */
	private int jobType;
	/**
	 * 重复次
	 */
	private int repeatCount;
	/**
	 * 间隔多少秒执行一次
	 */
	private int intervalInSeconds;

    private int successCount;

    private int failCount;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getScheduleName() {
		return scheduleName;
	}

	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJobProviderBeanName() {
		return jobProviderBeanName;
	}

	public void setJobProviderBeanName(String jobProviderBeanName) {
		this.jobProviderBeanName = jobProviderBeanName;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(int jobStatus) {
		this.jobStatus = jobStatus;
	}

    public int getJobType() {
        return jobType;
    }

    public void setJobType(int jobType) {
        this.jobType = jobType;
    }

    public int getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
    }

    public int getIntervalInSeconds() {
        return intervalInSeconds;
    }

    public void setIntervalInSeconds(int intervalInSeconds) {
        this.intervalInSeconds = intervalInSeconds;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }
}
