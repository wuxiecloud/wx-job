package cn.xo68.boot.job;

import cn.xo68.boot.job.jdbcstore.SchedulerRegisterService;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;

/**
 * 任务调度器心跳
 * @author wuxie
 * @date 2018-7-9
 */
@Component
public class JobSchedulerHeartBeat implements Runnable {

    private static final Logger logger=LoggerFactory.getLogger(JobSchedulerHeartBeat.class);

    @Autowired
    private Scheduler scheduler;
    @Autowired
    private SchedulerRegisterService schedulerRegisterService;

    @Override
    public void run() {
        try{
            if(scheduler.isStarted() && !scheduler.isShutdown()){
                schedulerRegisterService.heartBeatReport(scheduler.getSchedulerName(), scheduler.getSchedulerInstanceId());
            }

        }catch (Exception e){
            logger.error("注册调度器心跳上报异常", e);
        }
    }
}
