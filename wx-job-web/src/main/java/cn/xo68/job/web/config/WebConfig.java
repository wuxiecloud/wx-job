package cn.xo68.job.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 *  webconfig
 */
@Configuration
public class WebConfig {

    @Bean
    public WebMvcConfigurer shiroMethodArgConfigurer() {

        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                // 设置了可以被跨域访问的路径和可以被哪些主机跨域访问
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedHeaders("Origin",
                                "X-Requested-With",
                                "Content-Type",
                                "Accept",
                                "Access-Control-Allow-Headers",
                                "Access-Control-Request-Method",
                                "Host",
                                "Authorization",
                                "x-access-token",
                                "Access-Control-Allow-Origin")
                        .allowedMethods("*")
                        .allowCredentials(true);
            }
        };
    }
}
