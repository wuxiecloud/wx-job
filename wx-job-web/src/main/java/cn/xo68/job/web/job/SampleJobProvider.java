package cn.xo68.job.web.job;

import cn.xo68.boot.job.JobProvider;
import cn.xo68.boot.job.entity.QuartzJob;
import cn.xo68.boot.webgather.resolve.htmlunit.GatherExecutor;
import cn.xo68.core.util.JsonUtil;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("sampleJobProvider")
public class SampleJobProvider implements JobProvider {

    private final static Logger logger = LoggerFactory.getLogger(SampleJobProvider.class);


    @Autowired(required = false)
    private GatherExecutor gatherExecutor;
    @Autowired
    private JsonUtil jsonUtil;


    @Override
    public void execute(JobExecutionContext context, QuartzJob quartzJob) {
        logger.info("大吉大利、今晚吃鸡,scheName:{},jobName:{},jobGroup: {}",quartzJob.getScheduleName(), quartzJob.getJobName(), quartzJob.getJobGroup());
    }
}
