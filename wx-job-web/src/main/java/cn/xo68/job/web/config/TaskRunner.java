package cn.xo68.job.web.config;

import cn.xo68.boot.job.entity.JobStatus;
import cn.xo68.boot.job.entity.JobType;
import cn.xo68.boot.job.entity.QuartzJob;
import cn.xo68.boot.job.jdbcstore.JobStoreService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * 初始化一个测试Demo任务
 * 创建者 科帮网
 * 创建时间	2018年4月3日
 */
@Component
public class TaskRunner implements ApplicationRunner{

    private final static Logger logger = LoggerFactory.getLogger(TaskRunner.class);

    @Autowired
    private JobStoreService jobStoreService;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void run(ApplicationArguments var) throws Exception{

        logger.info("初始化测试任务");
        QuartzJob quartzJob = new QuartzJob();
        quartzJob.setJobName("test0201");
        quartzJob.setJobGroup("testgroup");
        quartzJob.setDescription("分布式调度测试任务");
        quartzJob.setScheduleName("scheduler-demo2");
        quartzJob.setCronExpression("0/10 * * * * ?");
        quartzJob.setJobProviderBeanName("sampleJobProvider");
        quartzJob.setStartTime(new Date(System.currentTimeMillis()+1000*2));
        quartzJob.setParameters("");
        quartzJob.setJobType(JobType.CronSchedule.getCode());
        //quartzJob.setEndTime(new Date());
        quartzJob.setJobStatus(JobStatus.WaitRunning.getCode());

        try{
            jobStoreService.add(quartzJob);
        }catch (Exception e){
            logger.error("添加任务异常", e);
        }

    }

}