package cn.xo68.job.web.job;

import cn.xo68.boot.webgather.resolve.ContentResolve;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class BaiduListContentResolve implements ContentResolve {

    private String errMsg;

    private Document document;

    @Override
    public void setError(String err) {
        errMsg=err;
    }

    @Override
    public void setDocument(Document document) {
        this.document=document;
    }

    @Override
    public void setDocument(String documentString) {

    }

    public String getErrMsg() {
        return errMsg;
    }

    public Document getDocument() {
        return document;
    }

    @Override
    public Elements listElements(String cssQuery) {
        return null;
    }

    @Override
    public Element getElement(String cssQuery) {
        return null;
    }

    @Override
    public Element getElementById(String id) {
        return null;
    }
}
