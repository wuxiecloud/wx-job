CREATE TABLE `client_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` char(32) DEFAULT NULL,
  `user_name` varchar(64) DEFAULT NULL,
  `user_dept` varchar(64) DEFAULT NULL,
  `user_env` longtext,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;