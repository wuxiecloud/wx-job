//555x.org小说基础信息解析脚本
import cn.xo68.core.date.DateTime
import cn.xo68.core.util.StringTools
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import java.util.regex.Matcher
import java.util.regex.Pattern

Element infoElement = contentResolveDefault.getElement('div.bookcover')
Element titleElement = infoElement.selectFirst('h1.title')

String title = titleElement.text()
if(title!=null) {
    title= title.trim()
}

Elements infoElements= infoElement.select('p')
txtNovelDoc.setNovelName(title)
String author = infoElements.get(0).text().replace('作者：', '')
txtNovelDoc.setNovelAuthor(author)

String category= infoElements.get(1).text().replace('分类：', '')
txtNovelDoc.setNovelCategory(category)

String charCount = infoElements.get(2).text().replace('字数：', '').replace('字', '')
long lCharCount = Long.parseLong(charCount)
txtNovelDoc.setNovelCharacterCount(lCharCount)

String nStatus = infoElements.get(3).text().replace('进度：', '')
txtNovelDoc.setNovelStatus(nStatus)

String nuTime = infoElements.get(4).text().replace('更新：', '')
txtNovelDoc.setNovelUpdateTime(DateTime.Parse(nuTime, 'yyyy-MM-dd HH:mm').getDate())


Pattern pattern = Pattern.compile('(.*)/txt([1-9]\\d{1,10})\\.html')
Matcher matcher = pattern.matcher(txtNovelDoc.getNovelUrl())
String sourceNovelId = StringTools.EMPTY
if(matcher.matches() && matcher.groupCount() >= 2){
    sourceNovelId = matcher.group(2)
}
if(StringTools.isEmpty(sourceNovelId)){
    return ''
}else{
    return 'http://www.555x.org/home/down/txt/id/'+sourceNovelId
}