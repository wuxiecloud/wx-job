/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : wxauth

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2018-12-21 17:36:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `wx_job`
-- ----------------------------
DROP TABLE IF EXISTS `wx_job`;
CREATE TABLE `wx_job` (
  `job_id` char(32) NOT NULL DEFAULT '' COMMENT '任务编号',
  `schedule_name` varchar(256) DEFAULT NULL COMMENT '调度器名称',
  `job_name` varchar(256) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(256) DEFAULT NULL COMMENT '任务分组',
  `description` varchar(2000) DEFAULT NULL COMMENT '任务描述',
  `job_provider_bean_name` varchar(256) DEFAULT NULL COMMENT '任务提供者beanName',
  `cron_expression` varchar(256) DEFAULT NULL COMMENT '执行时间',
  `parameters` text COMMENT '参数',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `job_status` int(11) DEFAULT NULL COMMENT '任务状态',
  `job_type` int(11) DEFAULT NULL COMMENT '任务类型',
  `repeat_count` int(11) DEFAULT NULL COMMENT '重复次',
  `interval_in_seconds` int(11) DEFAULT NULL COMMENT '间隔多少秒执行一次',
  `success_count` int(11) DEFAULT '0' COMMENT '成功次数',
  `fail_count` int(11) DEFAULT '0' COMMENT '失败次数',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of wx_job
-- ----------------------------
INSERT INTO `wx_job` VALUES ('127c7cf1148e70e8f9cb191f382340c4', 'scheduler-demo2', '555x.org小说文本下载2', '文本小说', '表达式任务测试', 'txtNovelDownloadJobProvider', '0/10 * * * * ?', '{\n  \"batchGatherCount\": 1,\n  \"maxDownloadRate\": 200000\n}', null, null, '104', '1', '500', '10', '5636', '0');
INSERT INTO `wx_job` VALUES ('1b4c115ef2eedb2dfce55fcba8e2d794', 'scheduler-demo2', '555x.org小说文本下载', '文本小说', '表达式任务测试', 'txtNovelDownloadJobProvider', '0/10 * * * * ?', '{\n  \"batchGatherCount\": 1,\n  \"maxDownloadRate\": 200000\n}', null, null, '104', '1', '0', '10', '3279', '0');
INSERT INTO `wx_job` VALUES ('1fc7e1698301b27e8454e472aa54b43c', 'scheduler-demo2', '页面内容页采集2', '列表内容页模式', '表达式任务测试', 'webContentPageGatherJobProvider', '0/30 * * * * ?', '{\"jobId\":\"c527b080766f5851398753bb3d02b9ce\",\"batchGatherCount\":3}', null, null, '104', '1', '0', '2', '1862', '0');
INSERT INTO `wx_job` VALUES ('34b6d58cc0a5ca9d6d4f47fd2ad9c76b', 'scheduler-demo2', '页面内容页采集', '列表内容页模式', '表达式任务测试', 'webContentPageGatherJobProvider', '0/30 * * * * ?', '{\"jobId\":\"c527b080766f5851398753bb3d02b9ce\",\"batchGatherCount\":3}', '2018-12-14 00:00:00', null, '104', '1', '0', '2', '1817', '1');
INSERT INTO `wx_job` VALUES ('7de707b9214bdcf6416ec749934d1069', 'scheduler-demo2', '555x.org小说文本下载3', '文本小说', '表达式任务测试', 'txtNovelDownloadJobProvider', '0/10 * * * * ?', '{\n  \"batchGatherCount\": 1,\n  \"maxDownloadRate\": 200000\n}', null, null, '104', '1', '200', '10', '2456', '0');
INSERT INTO `wx_job` VALUES ('8ad4906f360d1be81cfc00a9d3f33e39', 'scheduler-demo2', '555x.org小说链接采集-异世大陆', '文本小说', '表达式任务测试', 'txtNovelGatherJobProvider', '0/10 * * * * ?', '{\n  \"resolveType\": \"OK_HTTP\",\n  \"listPageGatherConfig\":{\n    \"url\": \"http://m.555x.org/html/kehuanxiaoshuo6/list_28_${pageIndex}.html\",\n  \"hasPage\": true,\n  \"minPageIndex\": 1,\n  \"maxPageIndex\": 121,\n  \"listQuery\": \".content .searchlist p.title a\"\n  }\n}', null, null, '200', '1', '1', '30', '12', '1');
INSERT INTO `wx_job` VALUES ('8b2cdc572f2f1b489722b3c492ea462f', 'scheduler-demo2', '列表链接采集2', '列表内容页模式', '表达式任务测试', 'webListPageGatherJobProvider', '0/10 * * * * ?', '{ \"resolveType\":\"OK_HTTP\",\"url\": \"https://blog.csdn.net/nnsword/article/list/${pageIndex}\", \"hasPage\": true, \"minPageIndex\": 10, \"maxPageIndex\": 20, \"listQuery\": \".article-list div h4 a\", \"contentTitle\": \"H1.title-article\", \"contentBody\": \"#article_content\" }', null, null, '200', '1', '1', '30', '11', '0');
INSERT INTO `wx_job` VALUES ('8f6b6f84e90d78531b2cfe08182a6bc4', 'scheduler-demo2', '表达式任务测试9', 'default', '表达式任务测试', 'sampleJobProvider', '0/15 * * * * ?', null, null, null, '104', '0', '1', '30', '433', '0');
INSERT INTO `wx_job` VALUES ('c527b080766f5851398753bb3d02b9ce', 'scheduler-demo2', '列表链接采集', '列表内容页模式', '列表链接采集', 'webListPageGatherJobProvider', '0/10 * * * * ?', '{ \"resolveType\":\"OK_HTTP\",\"url\": \"https://blog.csdn.net/nnsword/article/list/${pageIndex}\", \"hasPage\": true, \"minPageIndex\": 1, \"maxPageIndex\": 9, \"listQuery\": \".article-list div h4 a\", \"contentTitle\": \"H1.title-article\", \"contentBody\": \"#article_content\" }', null, null, '200', '1', '1', '30', '25', '5');
INSERT INTO `wx_job` VALUES ('fd8a26f2dc4d10be051924458f607cbc', 'scheduler-demo2', '555x.org小说链接采集-全部分类', '文本小说', '表达式任务测试', 'txtNovelGatherJobProvider', '0/10 * * * * ?', '{\n  \"resolveType\": \"OK_HTTP\",\n  \"listPageGatherConfig\": {\n    \"url\": \"http://m.555x.org/shuku/0_0_0_0_default_0_${pageIndex}.html\",\n    \"hasPage\": true,\n    \"minPageIndex\": 1,\n    \"maxPageIndex\": 1308,\n    \"listQuery\": \"ul.book_list a\"\n  },\n  \"saveFolder\": \"D:\\\\tempdir\\\\novel2\\\\\",\n  \"infoResolveScript\": \"import cn.xo68.boot.webgather.resolve.ContentResolve;\\r\\n import org.jsoup.nodes.Element;\\r\\n import org.jsoup.select.Elements;\\r\\n import cn.xo68.boot.webgather.document.txtnovel.TxtNovelDoc;\\r\\n import cn.xo68.core.util.StringTools;\\r\\n import cn.xo68.core.date.DateTime;\\r\\n import java.util.regex.Matcher;\\r\\n import java.util.regex.Pattern;\\r\\n \\r\\n Element infoElement = contentResolveDefault.getElement(\'div.bookcover\');\\r\\n Element titleElement = infoElement.selectFirst(\'h1.title\');\\r\\n String title = titleElement.text();\\r\\n if(title!=null) {\\r\\n   title= title.trim();\\r\\n }\\r\\n Elements infoElements= infoElement.select(\'p\');\\r\\n txtNovelDoc.setNovelName(title);\\r\\n String author = infoElements.get(0).text().replace(\'作者：\', \'\');\\r\\n txtNovelDoc.setNovelAuthor(author);\\r\\n String category= infoElements.get(1).text().replace(\'分类：\', \'\');\\r\\n txtNovelDoc.setNovelCategory(category);\\r\\n String charCount = infoElements.get(2).text().replace(\'字数：\', \'\').replace(\'字\', \'\');\\r\\n long lCharCount = Long.parseLong(charCount);\\r\\n txtNovelDoc.setNovelCharacterCount(lCharCount);\\r\\n String nStatus = infoElements.get(3).text().replace(\'进度：\', \'\');\\r\\n txtNovelDoc.setNovelStatus(nStatus);\\r\\n String nuTime = infoElements.get(4).text().replace(\'更新：\', \'\');\\r\\n txtNovelDoc.setNovelUpdateTime(DateTime.Parse(nuTime, \'yyyy-MM-dd HH:mm\').getDate());\\r\\n Pattern pattern = Pattern.compile(\'(.*)/txt([1-9]\\\\\\\\d{1,10})\\\\\\\\.html\');\\r\\n Matcher matcher = pattern.matcher(txtNovelDoc.getNovelUrl());\\r\\n String sourceNovelId = StringTools.EMPTY;\\r\\n if(matcher.matches() && matcher.groupCount() >= 2){\\r\\n   sourceNovelId = matcher.group(2);\\r\\n }\\r\\n if(StringTools.isEmpty(sourceNovelId)){\\r\\n   return \'\';\\r\\n }else{\\r\\n   return \'http://www.555x.org/home/down/txt/id/\'+sourceNovelId;\\r\\n }\\r\\n\"\n}', null, null, '200', '1', '1', '30', '16', '1');
