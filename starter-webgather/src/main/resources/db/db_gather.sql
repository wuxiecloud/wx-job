CREATE TABLE `wx_gather_plan` (
  `plan_id` char(32) NOT NULL DEFAULT '',
  `plan_channel` varchar(32) DEFAULT NULL,
  `plan_name` varchar(256) DEFAULT NULL,
  `gather_type` varchar(32) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `pattern_query` varchar(256) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `wx_gather_record` (
  `record_id` char(32) NOT NULL DEFAULT '',
  `plan_id` char(32) DEFAULT NULL,
  `url` varchar(512) DEFAULT NULL,
  `pattern_query` varchar(256) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `gather_status` int(11) DEFAULT '0',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;