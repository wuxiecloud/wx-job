package cn.xo68.boot.webgather.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * 采集计划
 * @author wuxie
 * @date  2018-7-19
 */
public class GatherPlan implements Serializable {

    private String planId;
    /**
     * 计划频道
     */
    private String planChannel;
    /**
     * 计划名称
     */
    private String planName;
    /**
     * 采集类型
     */
    private String gatherType;

    /**
     * 采集地址
     */
    private String url;
    /**
     * 内容匹配查询
     */
    private String patternQuery;


    /**
     * 创建时间
     */
    private Date createTime;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getPlanChannel() {
        return planChannel;
    }

    public void setPlanChannel(String planChannel) {
        this.planChannel = planChannel;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getGatherType() {
        return gatherType;
    }

    public void setGatherType(String gatherType) {
        this.gatherType = gatherType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPatternQuery() {
        return patternQuery;
    }

    public void setPatternQuery(String patternQuery) {
        this.patternQuery = patternQuery;
    }



    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
