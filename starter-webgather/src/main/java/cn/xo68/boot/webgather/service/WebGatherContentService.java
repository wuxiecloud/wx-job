package cn.xo68.boot.webgather.service;

import cn.xo68.boot.webgather.document.WebGatherContentDoc;
import cn.xo68.boot.webgather.repository.WebGatherContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 采集页面内容服务层
 */
@Service
public class WebGatherContentService {

    @Autowired
    private WebGatherContentRepository webGatherContentRepository;

    public void insert(WebGatherContentDoc doc){
        webGatherContentRepository.insert(doc);
    }
}
