package cn.xo68.boot.webgather.resolve;

/**
 * 解析类型枚举
 * @author wuxie
 * @date 2018-12-14
 */
public enum ResolveTypeEnums {
    /**
     * okhttp加载解析
     */
    OK_HTTP,
    /**
     * htmlunit加载解析
     */
    HTML_UNIT;
}
