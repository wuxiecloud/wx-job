package cn.xo68.boot.webgather;

import cn.xo68.boot.webgather.job.WebContentPageGatherJobProvider;
import cn.xo68.boot.webgather.job.WebListPageGatherJobProvider;
import cn.xo68.boot.webgather.job.txtnovel.TxtNovelDownloadJobProvider;
import cn.xo68.boot.webgather.job.txtnovel.TxtNovelLinkGatherJobProvider;
import cn.xo68.boot.webgather.properties.WebGatherProperties;
import cn.xo68.boot.webgather.resolve.ResolveFactory;
import cn.xo68.boot.webgather.resolve.htmlunit.GatherExecutor;
import cn.xo68.boot.webgather.resolve.htmlunit.WebClientFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * web 收集自动配置
 */
@Configuration
@EnableConfigurationProperties({WebGatherProperties.class})
@ConditionalOnProperty(prefix = "wuxie.webgather",value = "enabled",havingValue = "true", matchIfMissing=true)
@EnableMongoRepositories(basePackages = {"cn.xo68.boot.webgather.repository"})
@EntityScan({"cn.xo68.boot.webgather.document"})
@ComponentScan({"cn.xo68.boot.webgather.service"})
@Import({ WebListPageGatherJobProvider.class,
        WebContentPageGatherJobProvider.class,
        TxtNovelLinkGatherJobProvider.class,
        TxtNovelDownloadJobProvider.class })
public class WebGatherAutoConfiguration {

    @ConditionalOnMissingBean(WebClientFactory.class)
    @Bean
    public WebClientFactory webClientFactory(WebGatherProperties webGatherProperties){
        WebClientFactory webClientFactory=new WebClientFactory();
        webClientFactory.setWebGatherProperties(webGatherProperties);
        return webClientFactory;
    }

    @ConditionalOnMissingBean(GatherExecutor.class)
    @Bean
    public GatherExecutor gatherExecutor(WebClientFactory webClientFactory){
        return new GatherExecutor(webClientFactory);
    }

    @ConditionalOnMissingBean(ResolveFactory.class)
    @Bean
    public ResolveFactory resolveFactory(GatherExecutor gatherExecutor){
        return new ResolveFactory(gatherExecutor);
    }

}
