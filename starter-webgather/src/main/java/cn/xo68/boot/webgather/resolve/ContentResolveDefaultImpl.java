package cn.xo68.boot.webgather.resolve;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 内容解析默认实现
 * @author  wuxie
 * @date 2018-7-18
 */
public class ContentResolveDefaultImpl implements ContentResolve {

    private String errMsg;

    private Document document;


    @Override
    public void setError(String err) {
        errMsg=err;
    }

    @Override
    public void setDocument(Document document) {
        this.document=document;
    }

    @Override
    public void setDocument(String documentString) {
        this.document= Jsoup.parse(documentString);
    }

    @Override
    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public Document getDocument() {
        return document;
    }

    @Override
    public Elements listElements(String cssQuery){
        Elements eles = document.select(cssQuery);
        return eles;
    }
    @Override
    public Element getElement(String cssQuery){
        Elements eles = document.select(cssQuery);
        return eles.first();
    }
    @Override
    public Element getElementById(String id){
       return document.getElementById(id);
    }
}
