package cn.xo68.boot.webgather.repository;

import cn.xo68.boot.webgather.common.GatherStatusEnums;
import cn.xo68.boot.webgather.document.WebGatherLinkDoc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * 列表链接仓储
 */
@Repository
public interface WebGatherLinkRepository extends MongoRepository<WebGatherLinkDoc,String>{

    WebGatherLinkDoc findFirstByLinkUrl(String linkUrl);

    @Query(value = "{gatherStatus:?0}")
    Page<WebGatherLinkDoc> findTopByJobIdAndGatherStatus(GatherStatusEnums gatherStatus, Pageable pageable);
}
