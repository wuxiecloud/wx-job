package cn.xo68.boot.webgather.http;

import okhttp3.Call;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 字符串回调
 *
 * @author wuyy cym
 * @date 2018年1月11日上午10:25:07
 */
public class StringHttpCallback implements HttpCallback {

    private final static Logger logger = LoggerFactory.getLogger(StringHttpCallback.class);

    private String result = null;
    private Throwable exception = null;


    public String getResult() {
        return result;
    }

    public Throwable getException() {
        return exception;
    }

    @Override
    public void success(Response response, Call call) {
        try {
            result = response.body().string();
        } catch (IOException e) {
            this.exception = e;
            logger.error("响应数据流处理异常", e);
        }
    }

    @Override
    public void failure(IOException e, Call call) {
        this.exception = e;
        logger.error("请求数据异常", e);
    }

    @Override
    public boolean hasException() {
        return exception!=null;
    }

}
