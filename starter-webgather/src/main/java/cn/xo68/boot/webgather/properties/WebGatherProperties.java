package cn.xo68.boot.webgather.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 分布式调度任务配置
 * @author wuxie
 * @date 2018-6-21
 */
@ConfigurationProperties("wuxie.webgather")
public class WebGatherProperties {

    private boolean enabled;

    private int webTimeout=10000;

    private int waitForBackgroundJavaScriptMillis=60000;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getWebTimeout() {
        return webTimeout;
    }

    public void setWebTimeout(int webTimeout) {
        this.webTimeout = webTimeout;
    }

    public int getWaitForBackgroundJavaScriptMillis() {
        return waitForBackgroundJavaScriptMillis;
    }

    public void setWaitForBackgroundJavaScriptMillis(int waitForBackgroundJavaScriptMillis) {
        this.waitForBackgroundJavaScriptMillis = waitForBackgroundJavaScriptMillis;
    }
}
