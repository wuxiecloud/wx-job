package cn.xo68.boot.webgather.document.txtnovel;

import java.io.Serializable;

/**
 * 文本小说下载配置
 * @author wuxie
 * @date 2018-12-14
 */
public class TxtNovelDownloadConfig implements Serializable {

    /**
     * 保存目录
     */
    private String saveFolder;
    /**
     * 每批采集链接数量
     */
    private Integer batchGatherCount;

    private long maxDownloadRate = 0L;

    public String getSaveFolder() {
        return saveFolder;
    }

    public void setSaveFolder(String saveFolder) {
        this.saveFolder = saveFolder;
    }

    public Integer getBatchGatherCount() {
        return batchGatherCount;
    }

    public void setBatchGatherCount(Integer batchGatherCount) {
        this.batchGatherCount = batchGatherCount;
    }

    public long getMaxDownloadRate() {
        return maxDownloadRate;
    }

    public void setMaxDownloadRate(long maxDownloadRate) {
        this.maxDownloadRate = maxDownloadRate;
    }
}
