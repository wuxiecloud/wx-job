package cn.xo68.boot.webgather.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 页面内容采集信息
 */
@Document(collection = "gather_content")
public class WebGatherContentDoc  implements Serializable {

    @Id
    private String contentId;

    @Indexed
    private String linkUrl;

    private String contentTitle;

    private String contentBody;

    private Date contentGatherTime;

    private Map<String,Object> otherProperties;

    public String getContentId() {
        return contentId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getContentBody() {
        return contentBody;
    }

    public void setContentBody(String contentBody) {
        this.contentBody = contentBody;
    }

    public Date getContentGatherTime() {
        return contentGatherTime;
    }

    public void setContentGatherTime(Date contentGatherTime) {
        this.contentGatherTime = contentGatherTime;
    }

    public Map<String, Object> getOtherProperties() {
        return otherProperties;
    }

    public void setOtherProperties(Map<String, Object> otherProperties) {
        this.otherProperties = otherProperties;
    }
}
