package cn.xo68.boot.webgather.entity;

import java.io.Serializable;

/**
 * 采集内容页面配置
 * @author wuxie
 * @date 2018-12-13
 */
public class GatherContentPageConfig  implements Serializable {

    /**
     * 父任务编号
     */
    private String jobId;

    /**
     * 每批采集链接数量
     */
    private Integer batchGatherCount;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public Integer getBatchGatherCount() {
        return batchGatherCount;
    }

    public void setBatchGatherCount(Integer batchGatherCount) {
        this.batchGatherCount = batchGatherCount;
    }

    @Override
    public String toString() {
        return "GatherContentPageConfig{" +
                "jobId='" + jobId + '\'' +
                ", batchGatherCount=" + batchGatherCount +
                '}';
    }
}
