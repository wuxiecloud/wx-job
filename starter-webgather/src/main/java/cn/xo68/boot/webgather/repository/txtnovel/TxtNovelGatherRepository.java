package cn.xo68.boot.webgather.repository.txtnovel;

import cn.xo68.boot.webgather.common.GatherStatusEnums;
import cn.xo68.boot.webgather.document.txtnovel.TxtNovelDoc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TxtNovelGatherRepository extends MongoRepository<TxtNovelDoc,String> {

    TxtNovelDoc findFirstByNovelUrl(String novelUrl);

    @Query(value = "{gatherStatus:?0}")
    Page<TxtNovelDoc> findTopByGatherStatus(GatherStatusEnums gatherStatus, Pageable pageable);
}
