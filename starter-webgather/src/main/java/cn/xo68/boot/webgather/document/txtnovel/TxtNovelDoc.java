package cn.xo68.boot.webgather.document.txtnovel;


import cn.xo68.boot.webgather.common.GatherStatusEnums;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 文本小说采集
 * @author wuxie
 * @date 2018-12-14
 */
@Document(collection = "txt_novel")
public class TxtNovelDoc implements Serializable {
    @Id
    private String novelId;

    private String siteDomain;

    private String novelName;

    private String novelAuthor;

    @Indexed
    private String novelCategory;

    private long novelCharacterCount;

    private long novelTxtSize;

    private String novelStatus;

    private String novelUrl;

    private TxtNovelLinkGatherConfig txtNovelLinkGatherConfig;

    @Indexed
    private GatherStatusEnums gatherStatus;

    @Indexed
    private Date linkGatherTime;

    /**
     * 小说更新时间
     */
    private Date novelUpdateTime;

    private Date contentStartGatherTime;
    private Date contentEndGatherTime;


    public String getNovelId() {
        return novelId;
    }

    public void setNovelId(String novelId) {
        this.novelId = novelId;
    }

    public String getSiteDomain() {
        return siteDomain;
    }

    public void setSiteDomain(String siteDomain) {
        this.siteDomain = siteDomain;
    }

    public String getNovelName() {
        return novelName;
    }

    public void setNovelName(String novelName) {
        this.novelName = novelName;
    }

    public String getNovelAuthor() {
        return novelAuthor;
    }

    public void setNovelAuthor(String novelAuthor) {
        this.novelAuthor = novelAuthor;
    }

    public String getNovelCategory() {
        return novelCategory;
    }

    public void setNovelCategory(String novelCategory) {
        this.novelCategory = novelCategory;
    }

    public long getNovelCharacterCount() {
        return novelCharacterCount;
    }

    public void setNovelCharacterCount(long novelCharacterCount) {
        this.novelCharacterCount = novelCharacterCount;
    }

    public long getNovelTxtSize() {
        return novelTxtSize;
    }

    public void setNovelTxtSize(long novelTxtSize) {
        this.novelTxtSize = novelTxtSize;
    }

    public String getNovelStatus() {
        return novelStatus;
    }

    public void setNovelStatus(String novelStatus) {
        this.novelStatus = novelStatus;
    }

    public String getNovelUrl() {
        return novelUrl;
    }

    public void setNovelUrl(String novelUrl) {
        this.novelUrl = novelUrl;
    }

    public TxtNovelLinkGatherConfig getTxtNovelLinkGatherConfig() {
        return txtNovelLinkGatherConfig;
    }

    public void setTxtNovelLinkGatherConfig(TxtNovelLinkGatherConfig txtNovelLinkGatherConfig) {
        this.txtNovelLinkGatherConfig = txtNovelLinkGatherConfig;
    }

    public GatherStatusEnums getGatherStatus() {
        return gatherStatus;
    }

    public void setGatherStatus(GatherStatusEnums gatherStatus) {
        this.gatherStatus = gatherStatus;
    }

    public Date getLinkGatherTime() {
        return linkGatherTime;
    }

    public void setLinkGatherTime(Date linkGatherTime) {
        this.linkGatherTime = linkGatherTime;
    }

    public Date getNovelUpdateTime() {
        return novelUpdateTime;
    }

    public void setNovelUpdateTime(Date novelUpdateTime) {
        this.novelUpdateTime = novelUpdateTime;
    }

    public Date getContentStartGatherTime() {
        return contentStartGatherTime;
    }

    public void setContentStartGatherTime(Date contentStartGatherTime) {
        this.contentStartGatherTime = contentStartGatherTime;
    }

    public Date getContentEndGatherTime() {
        return contentEndGatherTime;
    }

    public void setContentEndGatherTime(Date contentEndGatherTime) {
        this.contentEndGatherTime = contentEndGatherTime;
    }

    @Override
    public String toString() {
        return "TxtNovelDoc{" +
                "novelId='" + novelId + '\'' +
                ", siteDomain='" + siteDomain + '\'' +
                ", novelName='" + novelName + '\'' +
                ", novelAuthor='" + novelAuthor + '\'' +
                ", novelCategory='" + novelCategory + '\'' +
                ", novelCharacterCount=" + novelCharacterCount +
                ", novelTxtSize=" + novelTxtSize +
                ", novelStatus='" + novelStatus + '\'' +
                ", novelUrl='" + novelUrl + '\'' +
                ", txtNovelLinkGatherConfig=" + txtNovelLinkGatherConfig +
                ", gatherStatus=" + gatherStatus +
                ", linkGatherTime=" + linkGatherTime +
                ", novelUpdateTime=" + novelUpdateTime +
                ", contentStartGatherTime=" + contentStartGatherTime +
                ", contentEndGatherTime=" + contentEndGatherTime +
                '}';
    }
}
