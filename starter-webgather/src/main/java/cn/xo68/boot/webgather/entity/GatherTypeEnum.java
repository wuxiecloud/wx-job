package cn.xo68.boot.webgather.entity;

/**
 * 采集类型
 * @author wuxie
 * @date 2018-7-20
 */
public enum GatherTypeEnum {
    /**
     * 列表
     */
    LIST,
    /**
     * 详情
     */
    DETAIL;
}
