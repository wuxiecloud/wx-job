package cn.xo68.boot.webgather.common;

/**
 * 采集状态
 * @author wuxie
 * @date 2018-12-13
 */
public enum GatherStatusEnums {
    /**
     * 待采集
     */
    WAITGATHING,
    /**
     * 采集中
     */
    GATHING,
    /**
     * 采集成功
     */
    FINISH,
    /**
     * 采集失败
     */
    FAIL;
}
