package cn.xo68.boot.webgather.entity;

import java.io.Serializable;

/**
 * 列表页面链接采集配置
 */
public class ListPageGatherConfig implements Serializable {

    private String url;

    private boolean hasPage;

    private int minPageIndex;

    private int maxPageIndex;

    private String listQuery;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isHasPage() {
        return hasPage;
    }

    public void setHasPage(boolean hasPage) {
        this.hasPage = hasPage;
    }

    public int getMinPageIndex() {
        return minPageIndex;
    }

    public void setMinPageIndex(int minPageIndex) {
        this.minPageIndex = minPageIndex;
    }

    public int getMaxPageIndex() {
        return maxPageIndex;
    }

    public void setMaxPageIndex(int maxPageIndex) {
        this.maxPageIndex = maxPageIndex;
    }

    public String getListQuery() {
        return listQuery;
    }

    public void setListQuery(String listQuery) {
        this.listQuery = listQuery;
    }
}
