package cn.xo68.boot.webgather.repository;

import cn.xo68.boot.webgather.document.WebGatherContentDoc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 页面内容信息仓储
 */
@Repository
public interface WebGatherContentRepository extends MongoRepository<WebGatherContentDoc,String>{

}
