package cn.xo68.boot.webgather.document.txtnovel;

import cn.xo68.boot.webgather.entity.ListPageGatherConfig;
import cn.xo68.boot.webgather.resolve.ResolveTypeEnums;

import java.io.Serializable;

/**
 * 文本小说采集配置
 */
public class TxtNovelLinkGatherConfig implements Serializable {

    private ResolveTypeEnums resolveType;

    private ListPageGatherConfig listPageGatherConfig;
    private String saveFolder;
    private String infoResolveScript;


    public ResolveTypeEnums getResolveType() {
        return resolveType;
    }

    public void setResolveType(ResolveTypeEnums resolveType) {
        this.resolveType = resolveType;
    }

    public ListPageGatherConfig getListPageGatherConfig() {
        return listPageGatherConfig;
    }

    public void setListPageGatherConfig(ListPageGatherConfig listPageGatherConfig) {
        this.listPageGatherConfig = listPageGatherConfig;
    }

    public String getSaveFolder() {
        return saveFolder;
    }

    public void setSaveFolder(String saveFolder) {
        this.saveFolder = saveFolder;
    }

    public String getInfoResolveScript() {
        return infoResolveScript;
    }

    public void setInfoResolveScript(String infoResolveScript) {
        this.infoResolveScript = infoResolveScript;
    }

    @Override
    public String toString() {
        return "TxtNovelLinkGatherConfig{" +
                "resolveType=" + resolveType +
                ", listPageGatherConfig=" + listPageGatherConfig +
                ", infoResolveScript='" + infoResolveScript + '\'' +
                '}';
    }
}
