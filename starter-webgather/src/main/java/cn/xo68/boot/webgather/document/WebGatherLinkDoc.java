package cn.xo68.boot.webgather.document;


import cn.xo68.boot.webgather.common.GatherStatusEnums;
import cn.xo68.boot.webgather.entity.GatherListPageConfig;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * 列表链接信息
 * @author wuxie
 * @date 2018-12-13
 */
@Document(collection = "gather_link")
public class WebGatherLinkDoc implements Serializable {


    @Id
    private String linkId;
    @Indexed
    private String jobId;
    private GatherListPageConfig gatherListPageConfig;

    private String linkTitle;

    @Indexed
    private String linkUrl;
    @Indexed
    private GatherStatusEnums gatherStatus;
    @Indexed
    private Date linkGatherTime;

    private Date contentStartGatherTime;
    private Date contentEndGatherTime;


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public GatherListPageConfig getGatherListPageConfig() {
        return gatherListPageConfig;
    }

    public void setGatherListPageConfig(GatherListPageConfig gatherListPageConfig) {
        this.gatherListPageConfig = gatherListPageConfig;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public GatherStatusEnums getGatherStatus() {
        return gatherStatus;
    }

    public void setGatherStatus(GatherStatusEnums gatherStatus) {
        this.gatherStatus = gatherStatus;
    }

    public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public Date getLinkGatherTime() {
        return linkGatherTime;
    }

    public void setLinkGatherTime(Date linkGatherTime) {
        this.linkGatherTime = linkGatherTime;
    }

    public Date getContentStartGatherTime() {
        return contentStartGatherTime;
    }

    public void setContentStartGatherTime(Date contentStartGatherTime) {
        this.contentStartGatherTime = contentStartGatherTime;
    }

    public Date getContentEndGatherTime() {
        return contentEndGatherTime;
    }

    public void setContentEndGatherTime(Date contentEndGatherTime) {
        this.contentEndGatherTime = contentEndGatherTime;
    }
}
