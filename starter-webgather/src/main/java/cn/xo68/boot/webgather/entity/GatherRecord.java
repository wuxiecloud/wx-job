package cn.xo68.boot.webgather.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 详情采集记录
 * @author wuxie
 * @date 2018-7-23
 */
public class GatherRecord  implements Serializable {

    private String recordId;

    private String planId;

    /**
     * 采集地址
     */
    private String url;
    /**
     * 内容匹配查询
     */
    private String patternQuery;

    /**
     * 采集状态,0:待采集，1：采集中，200：采集成功，400：采集失败
     */
    private int gatherStauts;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 开始采集时间
     */
    private Date gatherTime;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPatternQuery() {
        return patternQuery;
    }

    public void setPatternQuery(String patternQuery) {
        this.patternQuery = patternQuery;
    }

    public int getGatherStauts() {
        return gatherStauts;
    }

    public void setGatherStauts(int gatherStauts) {
        this.gatherStauts = gatherStauts;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getGatherTime() {
        return gatherTime;
    }

    public void setGatherTime(Date gatherTime) {
        this.gatherTime = gatherTime;
    }
}
