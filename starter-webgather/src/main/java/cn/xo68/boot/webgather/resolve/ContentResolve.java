package cn.xo68.boot.webgather.resolve;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 采集到内容解析接口
 */
public interface ContentResolve {

    /**
     * 发生错误
     * @param err
     */
    void setError(String err);

    /**
     * 请求后得到的文档信息
     * @param document
     */
    void setDocument(Document document);

    /**
     * 设置 请求后得到的文档信息
     * @param documentString
     */
    void setDocument(String documentString);

    String getErrMsg();

    Document getDocument();

    Elements listElements(String cssQuery);

    Element getElement(String cssQuery);

    Element getElementById(String id);
}
