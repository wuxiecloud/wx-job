package cn.xo68.boot.webgather.service.txtnovel;

import cn.xo68.boot.webgather.common.GatherStatusEnums;
import cn.xo68.boot.webgather.document.txtnovel.TxtNovelDoc;
import cn.xo68.boot.webgather.repository.txtnovel.TxtNovelGatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TxtNovelGatherService {

    @Autowired
    private TxtNovelGatherRepository txtNovelGatherRepository;

    public void insert(TxtNovelDoc doc){
        txtNovelGatherRepository.insert(doc);
    }

    public boolean exist(String novelUrl){
        TxtNovelDoc doc = txtNovelGatherRepository.findFirstByNovelUrl(novelUrl);
        return  doc !=null;
    }

    public void save(TxtNovelDoc entity){
        txtNovelGatherRepository.save(entity);
    }
    public void saves(Iterable<TxtNovelDoc> entities){
        txtNovelGatherRepository.saveAll(entities);
        //webGatherRepository.f

    }

    public Page<TxtNovelDoc> findTop(GatherStatusEnums gatherStatus, int batchGatherCount){
        PageRequest pageable =PageRequest.of(0, batchGatherCount, Sort.by(Sort.Order.asc("linkGatherTime")));

        return txtNovelGatherRepository.findTopByGatherStatus(gatherStatus, pageable);
    }
}
