package cn.xo68.boot.webgather.http;

import okhttp3.Call;
import okhttp3.Response;

import java.io.IOException;

/**
 * http 请求回调
 * @author wuyy
 * @date 2018年1月11日上午10:01:48
 *
 */
public interface HttpCallback {

    void success(Response response, Call call);
    void failure(IOException e, Call call);
    boolean hasException();
}
