package cn.xo68.boot.webgather.entity;

import cn.xo68.boot.webgather.resolve.ResolveTypeEnums;

import java.io.Serializable;


/**
 * 采集列表页面配置
 * @author wuxie
 * @date 2018-12-12
 */
public class GatherListPageConfig implements Serializable {

    private ResolveTypeEnums resolveType;

    private String url;

    private boolean hasPage;

    private int minPageIndex;

    private int maxPageIndex;

    private String listQuery;

    private String contentTitle;

    private String contentBody;

    public ResolveTypeEnums getResolveType() {
        return resolveType;
    }

    public void setResolveType(ResolveTypeEnums resolveType) {
        this.resolveType = resolveType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isHasPage() {
        return hasPage;
    }

    public void setHasPage(boolean hasPage) {
        this.hasPage = hasPage;
    }

    public int getMinPageIndex() {
        return minPageIndex;
    }

    public void setMinPageIndex(int minPageIndex) {
        this.minPageIndex = minPageIndex;
    }

    public int getMaxPageIndex() {
        return maxPageIndex;
    }

    public void setMaxPageIndex(int maxPageIndex) {
        this.maxPageIndex = maxPageIndex;
    }

    public String getListQuery() {
        return listQuery;
    }

    public void setListQuery(String listQuery) {
        this.listQuery = listQuery;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getContentBody() {
        return contentBody;
    }

    public void setContentBody(String contentBody) {
        this.contentBody = contentBody;
    }

    @Override
    public String toString() {
        return "GatherListPageConfig{" +
                "resolveType=" + resolveType +
                ", url='" + url + '\'' +
                ", hasPage=" + hasPage +
                ", minPageIndex=" + minPageIndex +
                ", maxPageIndex=" + maxPageIndex +
                ", listQuery='" + listQuery + '\'' +
                ", contentTitle='" + contentTitle + '\'' +
                ", contentBody='" + contentBody + '\'' +
                '}';
    }
}
