package cn.xo68.boot.webgather.service;

import cn.xo68.boot.webgather.common.GatherStatusEnums;
import cn.xo68.boot.webgather.document.WebGatherLinkDoc;
import cn.xo68.boot.webgather.repository.WebGatherLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * 列表链接服务层
 */
@Service
public class WebGatherLinkService {
    @Autowired
    private WebGatherLinkRepository webGatherLinkRepository;

    public void insertLink(WebGatherLinkDoc doc){
        webGatherLinkRepository.insert(doc);
    }

    public boolean exist(String linkUrl){
        WebGatherLinkDoc doc = webGatherLinkRepository.findFirstByLinkUrl(linkUrl);
        return  doc !=null;
    }

    public Page<WebGatherLinkDoc> findTop(GatherStatusEnums gatherStatus, int batchGatherCount){
        PageRequest pageable =PageRequest.of(0, batchGatherCount, Sort.by(Sort.Order.asc("linkGatherTime")));

       return webGatherLinkRepository.findTopByJobIdAndGatherStatus(gatherStatus, pageable);
    }

    public void updateStatus(WebGatherLinkDoc entity){
        webGatherLinkRepository.save(entity);
        //webGatherRepository.f
    }

    public void batchUpdateStatus(Iterable<WebGatherLinkDoc> entities){
        webGatherLinkRepository.saveAll(entities);
        //webGatherRepository.f
    }
}
